pipeline {
   // Some global default variables
    environment {
        DOCKERHUB_CREDS = credentials('dockerhub')
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "10.0.0.10:8081"
        NEXUS_REPOSITORY = "maven-snapshots"
        NEXUS_CREDENTIAL_ID = "nexus-cred"
    }

    parameters {
        string (name: 'DOCKER_REG',       defaultValue: 'walidsaad',                   description: 'Docker registry')
        string (name: 'IMAGE_NAME',       defaultValue: 'training-app',                                     description: 'Docker Image Name')
        string (name: 'IMAGE_TAG',       defaultValue: 'v1.0',                                     description: 'Docker Image tag')
         string (name: 'PHPMYADMIN_PORT',       defaultValue: '8000',                                     description: 'PHPMyAdmin Port')
    }
    tools {
        // Get the Maven tool.
      // ** NOTE: This 'M3' Maven tool must be configured
      // **       in the global configuration.
        maven "maven3"
    }


    agent any
    stages {
   /*stage('Preparation') {
      // Get some code from a GitHub repository
      steps {
      git 'https://github.com/walidsaad/training-app.git'
   }
   }     */

 stage('Build') {
      steps {
      // Run the maven build
         sh "mvn -B -DskipTests -Dmaven.test.failure.ignore clean package"
      }
   }

   stage('Unit Test') {
      steps {
         sh "mvn test"
      }
   }

   stage('Results') {
      steps {
      junit '**/target/surefire-reports/TEST-*.xml'
      archiveArtifacts  'target/*.jar'
      }
   }


stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml";
                    filesByGlob = findFiles(glob: "target/*-jar-with-dependencies.${pom.packaging}");
                    // Print POM's Informations
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"

                    //Retrieve Artifact Path
                    artifactPath = filesByGlob[0].path;
                    artifactExists = fileExists artifactPath;

                    // Test if artifact exist
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";

                        //upload to Nexus
                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }



   stage('Build Docker Maven Image') {
   steps {
      // Run the docker build command
   echo "Build Docker Image Of Training-App"
   sh "docker build -t=\"${IMAGE_NAME}:${IMAGE_TAG}\" ."
   }
   }

stage('Push Images To DockerHUb') {
    steps {
   //Run docker push command
   echo "Login to Docker HUB"
   sh "docker login --username $DOCKERHUB_CREDS_USR --password $DOCKERHUB_CREDS_PSW"
   echo "Push App Image vers DockerHUb"
   sh "docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${DOCKER_REG}/${IMAGE_NAME}:${IMAGE_TAG}"
   sh "docker push ${DOCKER_REG}/${IMAGE_NAME}:${IMAGE_TAG}"
   }
   }

   stage('Deploy App') {
      steps {
         // Run the docker-compose up command
      echo "Login to Docker HUB"
      sh "DOCKER_REG=$DOCKER_REG IMAGE_NAME=$IMAGE_NAME BUILD_TAG=$IMAGE_TAG PHPMYADMIN_PORT=$PHPMYADMIN_PORT docker-compose up -d"
      sh "docker logs training-app-jenkinsfile_app_1"
      }
   }
}
}
